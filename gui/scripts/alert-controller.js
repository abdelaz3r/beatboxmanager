alertController = {
	content: undefined,
	queue: [],
	counter: 0,
	interval: 50,
	cursor: 0,

	init: function() {
		alertController.content = $('.alert');
		alertController.runtime();
	},

	// ajoute une alert à la liste d'attente
	add: function(alert) {
		alertController.queue.push(alert);
		alertController.cursor++;
	},

	// affiche la prochaine alert
	display: function() {
		var alert = alertController.queue[0];
		alertController.queue.shift();
		alertController.content.prepend('<div class="item item-' + alertController.cursor + '">' + alert + '</div>');
		alertController.counter++;

		var cursor = alertController.cursor;

		setTimeout(function() {
			alertController.hide($('.item-' + cursor));
		}, 2000);
	},

	// supprime une alert affichée
	hide: function(alert) {
		alert.css('display', 'none');
		alertController.counter--;
	},

	// lance l'affichage de la prochaine alert
	runtime: function() {
		setInterval(function() {
			if (alertController.queue.length > 0) {
				alertController.display();
			}
		}, alertController.interval);
	}
};