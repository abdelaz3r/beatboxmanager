import pyaudio, wave, time, sys
from datetime import datetime
import socket
import time
import json
import os

CHUNK = 8192
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100
RECORD_SECONDS = 5


HOST = "127.0.0.1"
PORT = 33333

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

#path = ""
path = "/Users/Jerem/Documents/Etudes/M1/Semestre Spring:Printemps 2016/Future User Interfaces (FUI) /BEATBOX-Manager/beatboxmanager/core/RecordSound/Music/"

p = pyaudio.PyAudio()

stream = p.open(format=FORMAT, channels = CHANNELS, rate = RATE, input = True, input_device_index = 0, frames_per_buffer = CHUNK)

print("Start recording")
sock.sendto(json.dumps({"type": "start-record"}), (HOST, PORT))
frames = []
for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
    #print i
    data = stream.read(CHUNK)
    frames.append(data)

print("stop-record recording")

nameFileOriginal = "sample"
nameFile = "sample"
extensionFile = ".wav"
i = 0
while(os.path.isfile(path + nameFile + extensionFile) == True):
    i+=1
    nameFile = nameFileOriginal + str(i)

v = nameFile + extensionFile
WAVE_OUTPUT_FILENAME = path + nameFile + extensionFile

stream.stop_stream()
stream.close()
p.terminate()

wf = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
wf.setnchannels(CHANNELS)
wf.setsampwidth(p.get_sample_size(FORMAT))
wf.setframerate(RATE)
wf.writeframes(b''.join(frames))
wf.close()

sock.sendto(json.dumps({"type": "stop-record"}), (HOST, PORT))
sock.sendto(json.dumps({"type": "add-track","name": v}), (HOST, PORT))
time.sleep(1)
print("done - result written to " + nameFile + extensionFile)




