/** 
 * Class Track
 **/

function Track() {
	this.key;
	this.label;
	this.src;
	this.audio;

	this.isMuted;
	this.isSelected;
	this.isPlaying;

	this.html;
}

/** 
 * Constructor
 **/

Track.create = function(name) {
	var t = new Track();
	t.key = App.tracks.push(t) - 1;

	t.label = name;
	t.src = './../core/RecordSound/Music/' + name;

	t.audio = new Audio();
	t.audio.src = t.src;
	t.audio.loop = true;
	t.audio.volume = 1;

	t.isMuted = false;
	t.isSelected = false;
	t.isPlaying = false;

	$('.loop-stage .track-container').append(
		'<div class="track" id="track-' + t.key + '">' +
			'<i class="icon material-icons">album</i>' +
			'<span class="label">' + t.label + '</span>' +
			'<i class="mute material-icons">volume_up</i>' +
		'</div>'
	);

	t.html = $('#track-' + t.key);

	return t;
}

/** 
 * Main functions
 **/

Track.prototype.appear = function() {
	this.html.animate({
		'opacity': 1
	}, 250);

	if (App.cursor == undefined) {
		this.select();
		App.cursor = this;
	}

	return this;
}

Track.prototype.remove = function() {
	this.html.css('display', 'none');

	return this;
}

Track.prototype.play = function() {
	this.audio.currentTime = 0
	this.audio.play();
	this.isPlaying = true;
}

Track.prototype.stop = function() {
	this.audio.pause();
	this.isPlaying = false;
}

Track.prototype.toggleMute = function() {
	if (this.isMuted) {
		this.unmute();
	} else {
		this.mute();
	}
}

Track.prototype.mute = function() {
	this.isMuted = true;
	this.html.find('.mute').html('volume_off');
	this.html.addClass('muted');
	this.audio.volume = 0;
}

Track.prototype.unmute = function() {
	this.isMuted = false;
	this.html.find('.mute').html('volume_up');
	this.html.removeClass('muted');
	this.audio.volume = 1;
}

Track.prototype.toggleSelect = function() {
	if (this.isSelected) {
		this.unselect();
	} else {
		this.select();
	}
}

Track.prototype.select = function() {
	this.isSelected = true;
	this.html.addClass('selected');
}

Track.prototype.unselect = function() {
	this.isSelected = false;
	this.html.removeClass('selected');
}