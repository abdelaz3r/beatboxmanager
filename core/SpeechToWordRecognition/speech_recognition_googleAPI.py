#!/usr/bin/python
import speech_recognition as sr
import socket
import time
import json
import sys

HOST = "127.0.0.1"
PORT = 33333

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

json_cmd = json.loads('{"record": "start-record", "start": "start-record", "run" : "start-record", "up": "up-cursor", "previous": "up-cursor", "left": "up-cursor", "down": "down-cursor", "next" : "down-cursor", "right" : "down-cursor", "mute": "mute-selection", "unmute" : "unmute-selection", "play" : "play", "go" : "play", "stop" : "stop", "break" : "stop", "silence" : "mute-selection" , "restart" : "restart", "reset" : "restart", "clear" : "restart", "reboot" : "restart"}')

class speechGoogleApi():

    r = sr.Recognizer()
    m = sr.Microphone()
    second = 3
    #listWords = ["down","left","next","play","remove","right","save","stop","up","mute", "delete", "record", "previous", "unmute"]

    def setSecond(self, s):
        self.second = s
    def getSecond(self, s):
        return (self.second)
    def addWord(self, word):
        listWords.append(word)
    def getList(self, word):
        return listWords
    def setList(self, l):
        listWords = l

    def run(self):
        try:
            with self.m as source:
                self.r.adjust_for_ambient_noise(source)
                while True:
                    print("Recording ...")
                    audio = self.r.record(source, self.second) 
                    try:
                        # recognize speech using Google Speech Recognition
                        value = self.r.recognize_google(audio)
                        print value
                        i=0
                        for key in json_cmd.keys():
                            if(key == value):
                                sock.sendto(str.encode(json.dumps({"type": json_cmd[key]})), (HOST, PORT))
                                print(json_cmd[key])
                            i+=1
                    except sr.UnknownValueError:
                        print("Try again")
                    except sr.RequestError as e:
                        print("Uh oh! Couldn't request results from Google Speech Recognition service; {0}".format(e))

        except KeyboardInterrupt:
            pass

if __name__ == '__main__':

    try:
        x = speechGoogleApi()
        x.run()
    except Exception:
        sys.exc_clear()


