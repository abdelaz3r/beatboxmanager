'use strict';

var ps = require('python-shell');
var dgram = require('dgram');

var PORT = 33333;
var HOST = '127.0.0.1';

var server = dgram.createSocket('udp4');
var address = undefined;

jQuery(document).ready(function($) {
	// demarrage du serveur
	address = server.bind(PORT, HOST);

	// demarrage du gestionnaire d'alerte
	alertController.init();
	
	// lancement de python
	// WARNING WE NEED TO CHANGE PYTHON PATH (You may will do  "which python" in the terminal)
	var pythonPath = '/usr/local/bin/python';
	var options = {
		mode: 'text',
		pythonPath: pythonPath,
		pythonOptions: ['-u'],
		scriptPath: 'beatboxmanager/core/SpeechToWordRecognition/',
	};

	ps.run('speech_recognition_googleAPI.py', options, function (err, results) {
		if (err) throw err;
	});

	var options = {
		mode: 'text',
		pythonPath: pythonPath,
		pythonOptions: ['-u'],
		scriptPath: 'beatboxmanager/core/Gesture_recognition/',
	};

	ps.run('gesture_class.py', options, function (err, results) {
		if (err) throw err;
	});
});

// python events
server.on('listening', function () {
	var address = server.address();
	console.log('UDP Server listening on ' + address.address + ":" + address.port);
});

server.on('message', function (message, remote) {
	var m = JSON.parse(message);

	switch(m.type) {
		case 'start-record': 		App.toogleRecord(); break;
		case 'stop-record': 		App.toogleRecord(); break;
		case 'add-track': 			App.addTrack(m.name); break;
		case 'up-cursor': 			App.selectPrev(); break;
		case 'down-cursor': 		App.selectNext(); break;
		case 'switch-selection': 	App.switchSelection(); break;
		case 'mute-selection': 		App.muteSelection(); break;
		case 'unmute-selection': 	App.unmuteSelection(); break;
		case 'play': 				App.playMusic(); break;
		case 'stop': 				App.stopMusic(); break;
		case 'toogle-mute': 		App.toogleMuteSelection(); break;
		case 'restart': 			App.tryRestart(); break;
		case 'toogle-play':
			if (App.modeRestart) {
				App.commitRestart();
			} else {
				App.toogleMusic();
			}
		break;
	}

	switch(m.type) {
		case 'start-record': 		alertController.add('start recording'); break;
		case 'up-cursor': 			alertController.add('move cursor to up'); break;
		case 'down-cursor': 		alertController.add('move cursor to down'); break;
		case 'switch-selection': 	alertController.add('switch selection mode'); break;
		case 'mute-selection': 		alertController.add('mute selected track'); break;
		case 'unmute-selection': 	alertController.add('unmute selected track'); break;
		case 'play': 				alertController.add('play tracks'); break;
		case 'stop': 				alertController.add('stop tracks'); break;
		case 'toogle-play': 		alertController.add('play/stop tracks'); break;
		case 'toogle-mute': 		alertController.add('un/mute'); break;
	}
});