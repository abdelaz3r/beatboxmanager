#!/usr/bin/python
#It's base on work of https://github.com/vipul-sharma20/gesture-opencv
import cv2
import numpy as np
import math
import time
import os
import socket
import json
#from recordSound import recordSound 

HOST = "127.0.0.1"
PORT = 33333

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

#We need White background
class gesture : 

    def __init__(self):
        self.cap = cv2.VideoCapture(0)
        self.t = time.time()
        self.secondsCounter = time.time() # timestamps
        t = time.time()
        self.secondsCounter2Compare = [t,t,t,t,t]
        self.listCommandes = ["","","","","'/Users/Jerem/Documents/Etudes/M1/Semestre Spring:Printemps 2016/Future User Interfaces (FUI) /BEATBOX-Manager/beatboxmanager/core/RecordSound/recordSound2.py'",""]
        self.commandesIsSend = False
        self.camCoordX = 300
        self.camCoordY = 500
        self.camCoordX1 = 0
        self.camCoordY1 = 0
        self.numberOfSeconds = 3

    def config(self,x = 300, y= 500, x1 =0, y1=0):
	self.camCoordX = x
	self.camCoordY = y
	self.camCoordX1 = x1
	self.camCoordY1 = y1

    def setNumberOfSecond(self,s):
        self.numberOfSeconds = s

    def acceptCommand(self,numCmd):
        #print "Number fingers :",(numCmd + 1)
        val = (time.time() - self.secondsCounter2Compare[numCmd])
        if(val <= 1):
            self.secondsCounter2Compare[numCmd] += (time.time() - self.secondsCounter2Compare[numCmd])
            val = 0
        if((self.secondsCounter2Compare[numCmd] > self.secondsCounter + self.numberOfSeconds - 0.5) and self.commandesIsSend == False):
            self.commandesIsSend = True
            print "Apply commandes ",(numCmd+1)

    def run(self):
        while(self.cap.isOpened()):
            ret, img = self.cap.read()
            cv2.rectangle(img,(self.camCoordX,self.camCoordY),(self.camCoordX1,self.camCoordY1),(0,255,0),0)
            # x,y of the point on the left top and x1,y1 for the point on the bottom right
            crop_img = img[self.camCoordY1:self.camCoordY, self.camCoordX1:self.camCoordX]
	       # From the cv2.rectangle [y1:y , x1:x]
            grey = cv2.cvtColor(crop_img, cv2.COLOR_BGR2GRAY)
            value = (35, 35)
            blurred = cv2.GaussianBlur(grey, value, 0)
            _, thresh1 = cv2.threshold(blurred, 127, 255,
                                       cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
            # cam is in black and white
            cv2.imshow('Thresholded', thresh1)
            contours, hierarchy = cv2.findContours(thresh1.copy(),cv2.RETR_TREE, \
                    cv2.CHAIN_APPROX_NONE)
            max_area = -1
            for i in range(len(contours)):
                cnt=contours[i]
                area = cv2.contourArea(cnt)
                if(area>max_area):
                    max_area=area
                    ci=i
            cnt=contours[ci]
            x,y,w,h = cv2.boundingRect(cnt)
            cv2.rectangle(crop_img,(x,y),(x+w,y+h),(0,0,255),0)
            hull = cv2.convexHull(cnt)
            drawing = np.zeros(crop_img.shape,np.uint8)
            cv2.drawContours(drawing,[cnt],0,(0,255,0),0)
            cv2.drawContours(drawing,[hull],0,(0,0,255),0)
            hull = cv2.convexHull(cnt,returnPoints = False)
            defects = cv2.convexityDefects(cnt,hull)
            count_defects = 0
            cv2.drawContours(thresh1, contours, -1, (0,255,0), 3)
            for i in range(defects.shape[0]):
                #Each 5 seconds we reset counter
                #print secondsCounter
                if ((self.secondsCounter + self.numberOfSeconds) <=  time.time()): 
                        self.commandesIsSend = False
                        t = time.time()
                        self.secondsCounter = t
                        self.secondsCounter2Compare = [t,t,t,t,t]

                s,e,f,d = defects[i,0]
                start = tuple(cnt[s][0])
                end = tuple(cnt[e][0])
                far = tuple(cnt[f][0])
                a = math.sqrt((end[0] - start[0])**2 + (end[1] - start[1])**2)
                b = math.sqrt((far[0] - start[0])**2 + (far[1] - start[1])**2)
                c = math.sqrt((end[0] - far[0])**2 + (end[1] - far[1])**2)
                angle = math.acos((b**2 + c**2 - a**2)/(2*b*c)) * 60
                if angle <= 95:
                    count_defects += 1
                    cv2.circle(crop_img,far,1,[0,0,255],-1)
                cv2.line(crop_img,start,end,[0,255,0],2)

            # 3 Fingers
            if count_defects == 2:
                cv2.putText(img, "Three", (5,50), 50, 1, 1)
                self.acceptCommand(count_defects)
                if(self.commandesIsSend == True) :
                    #print "ok"
                    sock.sendto(json.dumps({"type": "switch-selection"}), (HOST, PORT))
                    self.commandesIsSend = False
                    time.sleep(0.5)
            # 4 Fingers
            elif count_defects == 3:
                cv2.putText(img,"Four ", (50,50), 50, 1, 1)
                self.acceptCommand(count_defects)
                if(self.commandesIsSend == True) :
                    sock.sendto(json.dumps({"type": "toogle-play"}), (HOST, PORT))
                    #sock.sendto(json.dumps({"type": "restart"}), (HOST, PORT))
                    self.commandesIsSend = False
                    time.sleep(0.5)
            # 5 Fingers
            elif count_defects == 4:
                cv2.putText(img,"Five", (50,50), 50, 1, 1)
                self.acceptCommand(count_defects)
                if(self.commandesIsSend == True) :
                    os.system("python " + self.listCommandes[count_defects])
                    self.commandesIsSend = False

            else:
                cv2.putText(img,"Commandes availables 3,4,5 fingers", (50,50),\
                            50, 1, 1)

            cv2.imshow('Gesture', img)
            # Time of refresh 
            k = cv2.waitKey(10)
            if k == 27:
                break

##################
##################
###### Main ###### 
##################
##################

if __name__ == '__main__':
    cam = gesture()
    cam.config(420,400,40,80)
    cam.run()




