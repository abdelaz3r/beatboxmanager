//
var App = {
	recording: false,
	recordingProcess: undefined,

	cursor: undefined,
	multipleSelection: false,

	modeRestart: false,

	tracks: [],

	_pulse: function(duration) {
		$('.recorder-stage .rythm-indicator').css({
			boxShadow: '0 0 0 0 rgba(36, 9, 76, .5), 0 0 0 0 rgba(36, 9, 76, .25), 0 0 0 0 rgba(36, 9, 76, .1)'
		}).animate({
			boxShadow: '0 0 0 30px rgba(36, 9, 76, 0), 0 0 0 60px rgba(36, 9, 76, 0), 0 0 0 90px rgba(36, 9, 76, 0)'
		}, duration);
	},

	_showTimer: function(seconds) {
		$('.recorder-stage .text-content').html('<i class="icon material-icons">settings_voice</i> ' + toMMSS(seconds));
	},

	toogleRecord: function() {
		var self = this;

		if (self.recording) {
			self.recording = false;

			$('.recorder-stage .text-content').html('Five fingers to record');

			clearInterval(self.recordingProcess);

			$('.recorder-stage .rythm-indicator').css({
				boxShadow: '0 0 0 0 rgba(36, 9, 76, .5), 0 0 0 0 rgba(36, 9, 76, .25), 0 0 0 0 rgba(36, 9, 76, .1)'
			});
		} else {
			self.stopMusic();

			self.recording = true;
			var seconds = 0;

			self._showTimer(seconds);
			self._pulse(900);
			
			self.recordingProcess = setInterval(function() {
				seconds++;
				self._showTimer(seconds);
				self._pulse(900);
			}, 1000);
		}
	},

	tryRestart: function() {
		var self = this;

		if (!self.modeRestart) {
			self.modeRestart = true;

			// modif visuelle
			$('.restart').animate({
				'opacity': '1'
			}, 500);

			// temps d'attente
			setTimeout(function() {
				if (self.modeRestart) {
					App.rollbackRestart();
				}
			}, 10000);
		}
	},

	rollbackRestart: function() {
		var self = this;

		if (self.modeRestart) {
			$('.restart').animate({
				'opacity': '0'
			}, 500);

			self.modeRestart = false;
		}
	},

	commitRestart: function() {
		var self = this;

		if (self.modeRestart) {
			for (var i = 0; i < App.tracks.length; i++) {
				App.tracks[i].remove();
			}

			App.tracks = [];

			$('.restart').animate({
				'opacity': '0'
			}, 500);

			self.modeRestart = false;
		}
	},

	addTrack: function(name) {
		var track = Track.create(name).appear();
		$('.recorder-stage .text-content').html('Five fingers to record');
	},

	selectNext: function() {
		var index = App.tracks.indexOf(App.cursor);

		if (!this.multipleSelection) {
			for (var i = 0; i < App.tracks.length; i++) {
				App.tracks[i].unselect();
			}
		}

		index = (index == App.tracks.length - 1) ? 0 : index + 1;

		App.cursor = App.tracks[index];
		App.tracks[index].select();
	},

	selectPrev: function() {
		var index = App.tracks.indexOf(App.cursor);

		if (!this.multipleSelection) {
			for (var i = 0; i < App.tracks.length; i++) {
				App.tracks[i].unselect();
			}
		}

		index = (index == 0) ? App.tracks.length - 1 : index - 1;

		App.cursor = App.tracks[index];
		App.tracks[index].select();
	},

	switchSelection: function() {
		if (this.multipleSelection) {
			this.multipleSelection = false;
			$('.loop-stage .selection-mode').html('Single selection mode');
		} else {
			this.multipleSelection = true;
			$('.loop-stage .selection-mode').html('Multiple selection mode');
		}
	},

	toogleMuteSelection: function() {
		for (var i = 0; i < App.tracks.length; i++) {
			if (App.tracks[i].isSelected) {
				App.tracks[i].toggleMute();
			}
		}
	},

	muteSelection: function() {
		for (var i = 0; i < App.tracks.length; i++) {
			if (App.tracks[i].isSelected) {
				App.tracks[i].mute();
			}
		}
	},

	unmuteSelection: function() {
		for (var i = 0; i < App.tracks.length; i++) {
			if (App.tracks[i].isSelected) {
				App.tracks[i].unmute();
			}
		}
	},

	playMusic: function() {
		$('.loop-stage .playing-status').html('Say stop');

		for (var i = 0; i < App.tracks.length; i++) {
			App.tracks[i].play();
		}
	},

	stopMusic: function() {
		$('.loop-stage .playing-status').html('Say play');

		for (var i = 0; i < App.tracks.length; i++) {
			App.tracks[i].stop();
		}
	},

	toogleMusic: function() {
		if (App.tracks.length > 0) {
			console.log(App.tracks[0]);
			if (App.tracks[0].isPlaying) {
				this.stopMusic();
			} else {
				this.playMusic();
			}
		}
	}
}